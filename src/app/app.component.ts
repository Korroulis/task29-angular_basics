import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-task29';
  public message ='I have to go get myself a Mac';
  public variable = 10;
  public arrays=[
    'I hate ',
    'changing ',
    'environment variables'
  ];
  public random = {
    score: 1000,
    result: true,
    greet: 'Hello'
  };

}
